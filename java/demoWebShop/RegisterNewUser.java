package demoWebShop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RegisterNewUser {

	public static void main(String[] args) {
		
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		//driver.manage().window().maximize();
		driver.findElement(By.xpath("//a[contains(text(),'Register')]")).click();
		driver.findElement(By.id("gender-male")).click();
		driver.findElement(By.id("FirstName")).sendKeys("David");
		driver.findElement(By.id("LastName")).sendKeys("Billa");
		driver.findElement(By.id("Email")).sendKeys("davidbilla07@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("David@07");
		driver.findElement(By.id("ConfirmPassword")).sendKeys("David@07");
		driver.findElement(By.id("register-button")).click();
	}

}
